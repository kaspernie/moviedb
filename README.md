# MovieDB


## Usages

### Online deployment

App is deployed on Heroku and its Swagger UI (exposing its CRUD capabilities) is found here:
[noroff-movie-db2.herokuapp.com/swagger-ui/index.html](https://noroff-movie-db2.herokuapp.com/swagger-ui/index.html)

Postgres database is deployed with [ElephantSQL](https://www.elephantsql.com/)


### Local deployment with local Postgres database in Docker

Run a postgres database server in Docker with user "postgress" and password "mysecretpassword". This can be achieved with:
```bash
docker run -d --name postgres-demo -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_DB=moviedb -p 5432:5432 postgres:14-alpine
```

Clone or download project in e.g. IntelliJ. Change the following in `application.properties` in order to a use a local database:

```
spring.datasource.url: jdbc:postgresql://localhost:5432/moviedb?currentSchema=public
spring.datasource.username: postgres
spring.datasource.password: mysecretpassword
```

Then build/run. This will create the database tables and seed data.


## Database diagram

![](moviedb_diagram.png)


## Credits

Project structure and code adapted from https://gitlab.com/muskatel/class_db_example by @muskatel, [course notes](https://noroff-accelerate.gitlab.io/java/course-notes/16_JPAHibernate/), and [Amigoscode Spring Boot Tutorial](https://www.youtube.com/watch?v=8SGI_XS5OPw), [Stackoverflow.com](https://stackoverflow.com/) and other sources.

The `build.gradle` file is adapted from https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci by @EternalDeiwos.


## Contributors

- Liv Reinhold (@livstella)
- Kasper Nielsen (@kaspernie)
