-- truncate/empty tables
TRUNCATE character_to_movie;
DELETE FROM character WHERE id>0;
DELETE FROM movie WHERE id>0;
DELETE FROM franchise WHERE id>0;

-- franchise: seed data

INSERT INTO franchise (id, description, name)
VALUES (1, 'In a galaxy full of dog shit', 'Dog Wars');

INSERT INTO franchise (id, description, name)
VALUES (2, 'May the force prosper in your favour, Harry! -Professor X', 'Lord of the Rings');


-- movie: seed data

INSERT INTO movie (id, director, genre, picture, release_year, title, trailer, franchise_id)
VALUES (1, 'Trunte', 'fantasy, thriller, romantic-comedy', 'https://cdn.shopify.com/s/files/1/0098/0828/1655/products/1102-3387-001_1_addcfcc9-e42e-4187-bca0-250a9359e668_500x.jpg?v=1635549321', 2017, 'Return of the Pug',
        'https://youtu.be/ooBQyT-vDdc', 1);

INSERT INTO movie (id, director, genre, picture, release_year, title, trailer, franchise_id)
VALUES (2, 'Trunte', 'fantasy, thriller, horror, comedy', 'https://i.kym-cdn.com/photos/images/original/001/058/158/dae.jpg', 2019, 'The Pure Awakens',
        'https://youtu.be/3uV4jwv_KfE', 1);

INSERT INTO movie (id, director, genre, picture, release_year, title, trailer, franchise_id)
VALUES (3, 'Peter Jackson', 'fantasy', 'https://images.platekompaniet.no/48cc22/globalassets/filmcover/2019/januar/n082100dsp01dvd2d.jpg', 2001, 'The Fellowship of the Ring',
        'https://www.youtube.com/watch?v=cKEGZ-CvWHk', 2);


-- character: seed data
INSERT INTO character (id, alias, full_name, gender, picture)
VALUES (1, 'Doggo', 'Trunte', 'Female', 'https://blog.tryfi.com/content/images/2021/03/Screen-Shot-2020-05-01-at-12.42.04-PM.png');

INSERT INTO character (id, alias, full_name, gender, picture)
VALUES (2, 'UnderDog', 'King', 'Male', 'https://images.baxterboo.com/global/images/products/large/big-dog-star-wars-darth-vader-dog-costume-1673.jpg');

INSERT INTO character (id, alias, full_name, gender, picture)
VALUES (3, 'Drogoson', 'Frodo Baggins', 'Male', 'https://i.imgur.com/6OhQcN2.jpeg');


-- character to move relations: seed

INSERT INTO character_to_movie (movie_id, character_id)
VALUES (1, 1);
INSERT INTO character_to_movie (movie_id, character_id)
VALUES (1, 2);
INSERT INTO character_to_movie (movie_id, character_id)
VALUES (2, 1);
INSERT INTO character_to_movie (movie_id, character_id)
VALUES (2, 2);
INSERT INTO character_to_movie (movie_id, character_id)
VALUES (3, 3);