package com.example.Controllers;

import com.example.Models.Franchise;
import com.example.Repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;


    // Create

    @PostMapping("/franchise/create")
    public Franchise createFranchise(@RequestBody Franchise franchise) {
        franchise = franchiseRepository.save(franchise);
        return franchise;
    }


    // Read

    @GetMapping("/franchise/{id}")
    public Franchise getFranchise(@PathVariable Long id) {
        Franchise franchise = null;
        if (franchiseRepository.existsById(id)) {
            // find will guarantee finding a unique franchise
            franchise = franchiseRepository.findById(id).get();
        }
        return franchise;
    }

    @GetMapping("/franchise/all")
    public List<Franchise> getAllFranchises() {
        return franchiseRepository.findAll();
    }


    // Update

    @PatchMapping("/franchise/{id}/update")
    public Franchise updateFranchise(
            @PathVariable Long id,
            @RequestBody Franchise franchise
    ) {
        if (franchiseRepository.existsById(id)) {
            franchise = franchiseRepository.save(franchise);
        }
        return franchise;
    }

    // Delete

    @DeleteMapping("/franchise/{id}/delete")
    public Franchise deleteFranchise(@PathVariable Long id) {
        if (franchiseRepository.existsById(id)) {
            franchiseRepository.deleteById(id);
        }
        return null;
    }
}