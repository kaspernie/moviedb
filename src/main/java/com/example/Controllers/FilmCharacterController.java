package com.example.Controllers;

import com.example.Models.FilmCharacter;
import com.example.Repositories.FilmCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FilmCharacterController {

    @Autowired
    private FilmCharacterRepository filmCharacterRepository;


    // Create

    @PostMapping("/character/create")
    public FilmCharacter createFilmCharacter(@RequestBody FilmCharacter filmCharacter) {
        filmCharacter = filmCharacterRepository.save(filmCharacter);
        return filmCharacter;
    }


    // Read

    @GetMapping("/character/{id}")
    // KN: testing ResponseEntity todo: keep/expand/remove ResponseEntity?
    public ResponseEntity<FilmCharacter> getFilmCharacter(@PathVariable Long id) {
        FilmCharacter filmCharacter = null;
        if (filmCharacterRepository.existsById(id)) {
            // find will guarantee finding a unique filmCharacter
            filmCharacter = filmCharacterRepository.findById(id).get();
        }
        return ResponseEntity.ok(filmCharacter);
    }

    @GetMapping("/character/all")
    public List<FilmCharacter> getAllFilmCharacters() {
        return filmCharacterRepository.findAll();
    }


    // Update

    @PatchMapping("/character/{id}/update")
    public FilmCharacter updateFilmCharacter(
            @PathVariable Long id,
            @RequestBody FilmCharacter filmCharacter
    ) {
        if (filmCharacterRepository.existsById(id)) {
            filmCharacter = filmCharacterRepository.save(filmCharacter);
        }
        return filmCharacter;
    }

    // Delete

    @DeleteMapping("/character/{id}/delete")
    public FilmCharacter deleteCharacter(@PathVariable Long id) {
        if (filmCharacterRepository.existsById(id)) {
            filmCharacterRepository.deleteById(id);
        }
        return null;
        // todo: useful return for deletion
    }
}