package com.example.Controllers;

import com.example.Models.Movie;
import com.example.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;


    // Create

    @PostMapping("/movie/create")
    public Movie createMovie(@RequestBody Movie movie) {
        movie = movieRepository.save(movie);
        return movie;
    }


    // Read

    @GetMapping("/movie/{id}")
    public Movie getMovie(@PathVariable Long id) {
        Movie movie = null;
        if (movieRepository.existsById(id)) {
            // find will guarantee finding a unique movie
            movie = movieRepository.findById(id).get();
        }
        return movie;
    }

    @GetMapping("/movie/all")
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }


    // Update

    @PatchMapping("/movie/{id}/update")
    public Movie updateMovie(
            @PathVariable Long id,
            @RequestBody Movie movie
    ) {
        if (movieRepository.existsById(id)) {
            movie = movieRepository.save(movie);
        }
        return movie;
    }

    // Delete

    @DeleteMapping("/movie/{id}/delete")
    public Movie deleteMovie(@PathVariable Long id) {
        if (movieRepository.existsById(id)) {
            movieRepository.deleteById(id);
        }
        return null;
        // todo: useful return statement for deletion
    }
}