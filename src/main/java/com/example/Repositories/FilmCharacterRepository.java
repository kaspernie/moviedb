package com.example.Repositories;

import com.example.Models.FilmCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmCharacterRepository extends JpaRepository<FilmCharacter, Long> {
}