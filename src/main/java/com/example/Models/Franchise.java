package com.example.Models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Franchise() {
    }

    @Id
    @SequenceGenerator(
            name = "franchise_sequence",
            sequenceName = "franchise_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.IDENTITY,
            generator = "franchise_sequence")
    public Long id;

    @OneToMany(mappedBy="franchise", fetch = FetchType.LAZY)
    Set<Movie> movies;

    @Column(unique = true)
    public String name;

    @Column
    public String description;

    public Long getId() {
        return id;
    }
}

