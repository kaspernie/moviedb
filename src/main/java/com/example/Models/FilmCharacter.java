package com.example.Models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = "Character")
public class FilmCharacter {

    public FilmCharacter(String fullName, String alias, String gender, String picture) {
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
    }

    public FilmCharacter() {
    }

    @Id
    @SequenceGenerator(
            name = "character_sequence",
            sequenceName = "character_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.IDENTITY,
            generator = "character_sequence"
    )
    public Long id;

    @ManyToMany(mappedBy = "filmCharacters", fetch = FetchType.LAZY)
    public Set<Movie> movies;
    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/movie/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    @Column
    public String fullName;

    @Column(nullable = true)
    public String alias;

    @Column
    public String gender;

    @Column
    public String picture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
