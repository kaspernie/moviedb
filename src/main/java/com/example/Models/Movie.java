package com.example.Models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {

    public Movie(String title, String genre, Integer releaseYear, String director, String picture, String trailer) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
    }

    public Movie() {
    }

    @Id
    @SequenceGenerator(
            name = "movie_generator",
            sequenceName = "movie_generator",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.IDENTITY,
            generator = "movie_generator"
    )
    public Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "character_to_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    public Set<FilmCharacter> filmCharacters;
    @JsonGetter("filmCharacters")
    public List<String> filmCharactersGetter() {
        if (filmCharacters != null) {
            return filmCharacters.stream()
                    .map(filmCharacter -> {
                        return "/character/" + filmCharacter.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
    @JsonGetter("franchise")
    public String franchise() {
        if(franchise != null){
            return "/franchise/" + franchise.getId();
        }else{
            return null;
        }
    }

    @Column(nullable = false)
    public String title;

    @Column
    public String genre;

    @Column
    public Integer releaseYear;

    @Column
    public String director;

    @Column
    public String picture;

    @Column
    public String trailer;

    public Long getId() {
        return id;
    }
}

