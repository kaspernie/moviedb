package com.example;

import com.example.Models.FilmCharacter;
import com.example.Repositories.FilmCharacterRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class MovieDbApi {

    public static void main(String[] args) {
        SpringApplication.run(MovieDbApi.class, args);
    }

    /*
    @Bean
    CommandLineRunner commandLineRunner(FilmCharacterRepository filmCharacterRepository) {
        return args -> {
            FilmCharacter testFilmCharacter = new FilmCharacter("Trunte", "Doggo","Female", "NOt-actually-a-url");
            filmCharacterRepository.save(testFilmCharacter);
        };
    }
    */
}

